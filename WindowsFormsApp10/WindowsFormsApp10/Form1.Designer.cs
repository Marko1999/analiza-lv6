﻿namespace WindowsFormsApp10
{
    partial class Kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bt_Zbroji = new System.Windows.Forms.Button();
            this.bt_Oduzmi = new System.Windows.Forms.Button();
            this.bt_Mnozi = new System.Windows.Forms.Button();
            this.bt_Dijeli = new System.Windows.Forms.Button();
            this.bt_Sin = new System.Windows.Forms.Button();
            this.bt_Cos = new System.Windows.Forms.Button();
            this.bt_Log = new System.Windows.Forms.Button();
            this.bt_Sqrt = new System.Windows.Forms.Button();
            this.tB1 = new System.Windows.Forms.TextBox();
            this.tB2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tB_Rezultat = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SuspendLayout();
            // 
            // bt_Zbroji
            // 
            this.bt_Zbroji.Location = new System.Drawing.Point(36, 268);
            this.bt_Zbroji.Name = "bt_Zbroji";
            this.bt_Zbroji.Size = new System.Drawing.Size(75, 23);
            this.bt_Zbroji.TabIndex = 0;
            this.bt_Zbroji.Text = "Zbroji";
            this.bt_Zbroji.UseVisualStyleBackColor = true;
            this.bt_Zbroji.Click += new System.EventHandler(this.button1_Click);
            // 
            // bt_Oduzmi
            // 
            this.bt_Oduzmi.Location = new System.Drawing.Point(36, 307);
            this.bt_Oduzmi.Name = "bt_Oduzmi";
            this.bt_Oduzmi.Size = new System.Drawing.Size(75, 23);
            this.bt_Oduzmi.TabIndex = 1;
            this.bt_Oduzmi.Text = "Oduzmi";
            this.bt_Oduzmi.UseVisualStyleBackColor = true;
            this.bt_Oduzmi.Click += new System.EventHandler(this.button2_Click);
            // 
            // bt_Mnozi
            // 
            this.bt_Mnozi.Location = new System.Drawing.Point(36, 349);
            this.bt_Mnozi.Name = "bt_Mnozi";
            this.bt_Mnozi.Size = new System.Drawing.Size(75, 23);
            this.bt_Mnozi.TabIndex = 2;
            this.bt_Mnozi.Text = "Mnozi";
            this.bt_Mnozi.UseVisualStyleBackColor = true;
            this.bt_Mnozi.Click += new System.EventHandler(this.bt_Mnozi_Click);
            // 
            // bt_Dijeli
            // 
            this.bt_Dijeli.Location = new System.Drawing.Point(36, 390);
            this.bt_Dijeli.Name = "bt_Dijeli";
            this.bt_Dijeli.Size = new System.Drawing.Size(75, 23);
            this.bt_Dijeli.TabIndex = 3;
            this.bt_Dijeli.Text = "Dijeli";
            this.bt_Dijeli.UseVisualStyleBackColor = true;
            this.bt_Dijeli.Click += new System.EventHandler(this.button4_Click);
            // 
            // bt_Sin
            // 
            this.bt_Sin.Location = new System.Drawing.Point(192, 268);
            this.bt_Sin.Name = "bt_Sin";
            this.bt_Sin.Size = new System.Drawing.Size(75, 23);
            this.bt_Sin.TabIndex = 4;
            this.bt_Sin.Text = "Sin(x)";
            this.bt_Sin.UseVisualStyleBackColor = true;
            this.bt_Sin.Click += new System.EventHandler(this.bt_Sin_Click);
            // 
            // bt_Cos
            // 
            this.bt_Cos.Location = new System.Drawing.Point(192, 307);
            this.bt_Cos.Name = "bt_Cos";
            this.bt_Cos.Size = new System.Drawing.Size(75, 23);
            this.bt_Cos.TabIndex = 5;
            this.bt_Cos.Text = "Cos(x)";
            this.bt_Cos.UseVisualStyleBackColor = true;
            this.bt_Cos.Click += new System.EventHandler(this.bt_Cos_Click);
            // 
            // bt_Log
            // 
            this.bt_Log.Location = new System.Drawing.Point(192, 348);
            this.bt_Log.Name = "bt_Log";
            this.bt_Log.Size = new System.Drawing.Size(75, 23);
            this.bt_Log.TabIndex = 6;
            this.bt_Log.Text = "Log(x)";
            this.bt_Log.UseVisualStyleBackColor = true;
            this.bt_Log.Click += new System.EventHandler(this.bt_Log_Click);
            // 
            // bt_Sqrt
            // 
            this.bt_Sqrt.Location = new System.Drawing.Point(192, 390);
            this.bt_Sqrt.Name = "bt_Sqrt";
            this.bt_Sqrt.Size = new System.Drawing.Size(75, 23);
            this.bt_Sqrt.TabIndex = 7;
            this.bt_Sqrt.Text = "Sqrt(x)";
            this.bt_Sqrt.UseVisualStyleBackColor = true;
            this.bt_Sqrt.Click += new System.EventHandler(this.bt_Sqrt_Click);
            // 
            // tB1
            // 
            this.tB1.Location = new System.Drawing.Point(36, 43);
            this.tB1.Name = "tB1";
            this.tB1.Size = new System.Drawing.Size(100, 20);
            this.tB1.TabIndex = 8;
            this.tB1.TextChanged += new System.EventHandler(this.tB1_TextChanged);
            // 
            // tB2
            // 
            this.tB2.Location = new System.Drawing.Point(177, 43);
            this.tB2.Name = "tB2";
            this.tB2.Size = new System.Drawing.Size(100, 20);
            this.tB2.TabIndex = 9;
            this.tB2.TextChanged += new System.EventHandler(this.tb2_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Prvi Broj";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(202, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Drugi Broj";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(128, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Rezultat:";
            // 
            // tB_Rezultat
            // 
            this.tB_Rezultat.Location = new System.Drawing.Point(104, 189);
            this.tB_Rezultat.Name = "tB_Rezultat";
            this.tB_Rezultat.Size = new System.Drawing.Size(100, 20);
            this.tB_Rezultat.TabIndex = 14;
            this.tB_Rezultat.TextChanged += new System.EventHandler(this.tB_Rezultat_TextChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 443);
            this.Controls.Add(this.tB_Rezultat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tB2);
            this.Controls.Add(this.tB1);
            this.Controls.Add(this.bt_Sqrt);
            this.Controls.Add(this.bt_Log);
            this.Controls.Add(this.bt_Cos);
            this.Controls.Add(this.bt_Sin);
            this.Controls.Add(this.bt_Dijeli);
            this.Controls.Add(this.bt_Mnozi);
            this.Controls.Add(this.bt_Oduzmi);
            this.Controls.Add(this.bt_Zbroji);
            this.Name = "Kalkulator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bt_Zbroji;
        private System.Windows.Forms.Button bt_Oduzmi;
        private System.Windows.Forms.Button bt_Mnozi;
        private System.Windows.Forms.Button bt_Dijeli;
        private System.Windows.Forms.Button bt_Sin;
        private System.Windows.Forms.Button bt_Cos;
        private System.Windows.Forms.Button bt_Log;
        private System.Windows.Forms.Button bt_Sqrt;
        private System.Windows.Forms.TextBox tB1;
        private System.Windows.Forms.TextBox tB2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tB_Rezultat;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}

