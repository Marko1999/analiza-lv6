﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp12
{
    public partial class Form1 : Form
    {
        private string path = "C:\\Users\\User\\Desktop\\a.txt";

        private string chosen = "";
        private string copchosen = "";



        private List<string> lista = new List<string>();
        private Random rng = new Random();
        private int broj_Pokusaja;
        



        public Form1()
        {
            InitializeComponent();
        }
        public void Reset()
        {
            chosen = lista[rng.Next(0, lista.Count - 1)];
            broj_Pokusaja = 6;
            

            
            textBox1.Clear();
            label2.Text = broj_Pokusaja.ToString();
            label4.Text = string.Empty;
            copchosen = "";
            for (int i = 0; i < chosen.Length; i++)
            {
                copchosen += "_";

            }
            follow();




        }
        public void follow()
        {
            label4.Text = "";
            for (int i = 0; i < copchosen.Length; i++)
            {
                label4.Text += copchosen.Substring(i, 1);
                label4.Text += " ";
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 1)
            {
                if (chosen.Contains(textBox1.Text))
                {
                    char[] temp = copchosen.ToCharArray();
                    char[] find = chosen.ToCharArray();
                    char charac = textBox1.Text.ElementAt(0);
                    for (int i = 0; i < find.Length; i++)
                    {
                        if (find[i] == charac)
                        {
                            temp[i] = charac;
                        }
                    }
                    copchosen = new string(temp);
                    follow();

                    label6.Text += textBox1.Text;
                    label6.Text += " ";
                    textBox1.Clear();

                }
                else
                {
                    --broj_Pokusaja;
                    
                    label6.Text += textBox1.Text;
                    label6.Text += " ";
                    textBox1.Clear();
                    label2.Text = broj_Pokusaja.ToString();
                    if (broj_Pokusaja == 0)
                    {
                        MessageBox.Show("GAME OVER!!");
                        DialogResult dialogResult = MessageBox.Show("Try Again?", "New Game", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {

                            Reset();
                        }
                        else
                        {
                            Application.Exit();
                        }
                    }

                }
            }
            else if (textBox1.Text == chosen)
            {
                textBox1.Clear();
                MessageBox.Show("POBJEDA!!");
            }
            else
            {
                --broj_Pokusaja;
               
                textBox1.Clear();
                if (broj_Pokusaja == 0)
                {
                    MessageBox.Show(string.Format("GAME OVER!!\n{0}", chosen));
                    DialogResult dialogResult = MessageBox.Show("Try Again?", "New Game", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {


                        Reset();
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
                label2.Text = broj_Pokusaja.ToString();
            }


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    lista.Add(line);
                }
                Reset();

            }

        }
    }
}
